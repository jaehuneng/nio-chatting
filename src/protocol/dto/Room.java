package protocol.dto;

public class Room {
    private int roomNumber;
    private String roomId;
    private int numOfUser;
    private int numOfText;

    public Room(int roomNumber, String roomId, int numOfUser, int numOfText) {
        this.roomNumber = roomNumber;
        this.roomId = roomId;
        this.numOfUser = numOfUser;
        this.numOfText = numOfText;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getNumOfText() {
        return numOfText;
    }

    public String getRoomId() {
        return roomId;
    }

    public int getNumOfUser() {
        return numOfUser;
    }

    public void setNumOfText(int numOfText) {
        this.numOfText = numOfText;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public void setNumOfUser(int numOfUser) {
        this.numOfUser = numOfUser;
    }
}
