package protocol.dto;

public class Response {
    private RequestResponse requestResponse;
    private BroadCastResponse broadCastResponse;

    public Response(RequestResponse response) {
        this.requestResponse = response;
    }

    public Response(BroadCastResponse response) {
        this.broadCastResponse = response;
    }

    public Response(RequestResponse requestResponse, BroadCastResponse broadCastResponse) {
        this.requestResponse = requestResponse;
        this.broadCastResponse = broadCastResponse;
    }

    public RequestResponse getRequestResponse() {
        return requestResponse;
    }

    public BroadCastResponse getBroadCastResponse() {
        return broadCastResponse;
    }

    public void setRequestResponse(RequestResponse requestResponse) {
        this.requestResponse = requestResponse;
    }

    public void setBroadCastResponse(BroadCastResponse broadCastResponse) {
        this.broadCastResponse = broadCastResponse;
    }

    //    ByteBuffer toBuffer();
}
