package protocol.dto;

import protocol.type.RequestType;

public class RequestResponse {
    private int requestId;
    private RequestType requestType;
    private int result;
    private Object data;

    public RequestResponse(int requestId, RequestType requestType, int result, Object data) {
        this.requestId = requestId;
        this.requestType = requestType;
        this.result = result;
        this.data = data;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public int getRequestId() {
        return requestId;
    }

    public int getResult() {
        return result;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public void setResult(int result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "Response{" +
                "requestId=" + requestId +
                ", requestType=" + requestType +
                ", result=" + result +
                ", data=" + data +
                '}';
    }
}
