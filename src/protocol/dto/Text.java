package protocol.dto;

public class Text {
    private int textId;
    private int numOfUnread;
    private int length;
    private String message;
    private String time;


    public Text(int textId, int numOfUnread, int length, String message) {
        this.textId = textId;
        this.numOfUnread = numOfUnread;
        this.length = length;
        this.message = message;
    }

    public int getLength() {
        return length;
    }

    public String getMessage() {
        return message;
    }

    public int getTextId() {
        return textId;
    }

    public int getNumOfUnread() {
        return numOfUnread;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setTextId(int textId) {
        this.textId = textId;
    }

    public void setNumOfUnread(int numOfUnread) {
        this.numOfUnread = numOfUnread;
    }

    public void read() {
        this.numOfUnread--;
    }

    @Override
    public String toString() {
        return "TextDto{" +
                "textId=" + textId +
                ", numOfUnread=" + numOfUnread +
                ", length=" + length +
                ", message='" + message + '\'' +
                '}';
    }
}
