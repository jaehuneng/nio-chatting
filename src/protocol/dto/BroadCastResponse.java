package protocol.dto;

import protocol.type.BroadCastType;

public class BroadCastResponse {
    private int requestId;
    private BroadCastType broadCastType;
    private int roomNumber;
    private String curId;
    private Object data;
    private boolean isRead;
    private String time;

    public BroadCastResponse(int requestId, BroadCastType broadCastType, int roomNumber, String curId, Object data, String time) {
        this.requestId = requestId;
        this.broadCastType = broadCastType;
        this.roomNumber = roomNumber;
        this.curId = curId;
        this.data = data;
        this.isRead = false;
        this.time = time;
    }

    public int getRequestId() {
        return requestId;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public BroadCastType getBroadCastType() {
        return broadCastType;
    }

    public void setBroadCastType(int broadCastNumber) {
        this.broadCastType = broadCastType;
    }

    public String getCurId() {
        return curId;
    }

    public void setCurId(String curId) {
        this.curId = curId;
    }

    public boolean isRead() {
        return isRead;
    }

    public void read() {
        this.isRead = true;
    }

    @Override
    public String toString() {
        return "BroadCastResponse{" +
                "requestId=" + requestId +
                ", broadCastType=" + broadCastType +
                ", roomNumber=" + roomNumber +
                ", curId='" + curId + '\'' +
                ", data=" + data +
                '}';
    }
}
