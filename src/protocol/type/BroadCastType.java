package protocol.type;

public enum BroadCastType {
    INVITE(0),
    QUIT(1),
    SEND(2),
    FILEUPLOAD(3),
    FILEREMOVE(4),
    ENTERROOM(5),
    SOCKETCLOSE(6);

    private int broadCastNumber;

    BroadCastType(int broadCastNumber) {
        this.broadCastNumber = broadCastNumber;
    }

    public int getBroadCastNumber() {
        return broadCastNumber;
    }

    public static BroadCastType findBroadCastType(int broadCastNumber) {
        for(BroadCastType broadCastType : BroadCastType.values()) {
            if (broadCastType.broadCastNumber == broadCastNumber) {
                return broadCastType;
            }
        }
        throw new IllegalArgumentException("잘못된 request가 들어옴!");
    }
}
