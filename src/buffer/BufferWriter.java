package buffer;

import protocol.dto.BroadCastResponse;
import protocol.dto.Request;
import protocol.dto.Room;
import protocol.dto.Text;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class BufferWriter {
    private static final Charset charset = Charset.forName("UTF-8");

    public static ByteBuffer makeBuffer(Request request) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(1000);

        byteBuffer.putInt(0, request.getRequestId());
        byteBuffer.putInt(4, request.getRequestType().getRequestNumber());

        putString(byteBuffer, request.getUserId(), 8, 23);

        byteBuffer.putInt(24, request.getRoomNumber());

        putData(byteBuffer, request.getData(), 28, 1000);

        byteBuffer.position(0);

        return byteBuffer;
    }

    public static ByteBuffer makeBuffer(BroadCastResponse response) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(1000);

        byteBuffer.putInt(0, -1);
        byteBuffer.putInt(4, response.getBroadCastType().getBroadCastNumber());
        byteBuffer.putInt(8, response.getRoomNumber());

        putString(byteBuffer, response.getCurId(), 12, 27);

        putString(byteBuffer, LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyMMddHHmm")), 27, 39);

        putData(byteBuffer, response.getData(), 40, 1000);

        byteBuffer.position(0);
        if (response.getData() == null) byteBuffer.limit(40);

        return byteBuffer;
    }

    private static void putString(ByteBuffer byteBuffer, String s, int start, int end) {
        if (s.length() + start > end) {
            throw new IllegalArgumentException("string 길이 초과!");
        }

        byteBuffer.position(start);
        byteBuffer.put(charset.encode(s));
    }

    private static void putData(ByteBuffer byteBuffer, Object data, int start, int end) {
        if (data == null) return;

        if (data instanceof String) {
            String text = (String) data;
            putString(byteBuffer, text, start, end);
            byteBuffer.limit(start + text.length());

        } else if (data instanceof String[]) {
            String[] l = (String[]) data;
            byteBuffer.putInt(start, l.length);

            int index = start + 4;
            for (int i=0; i<l.length; i++) {
                putString(byteBuffer, l[i], index, index + 16);
                index += 16;
            }

            byteBuffer.limit(index);

        } else if (data instanceof Integer) {
            byteBuffer.putInt(start, (int) data);
            byteBuffer.limit(start + 4);

        } else if (data instanceof Text) {
            Text text = (Text) data;

            byteBuffer.putInt(start, text.getTextId());
            byteBuffer.putInt(start + 4, text.getNumOfUnread());
            byteBuffer.putInt(start + 8, text.getLength());
            putString(byteBuffer, text.getMessage(), start + 12, start + 12 + text.getLength());

            byteBuffer.limit(start + 12 + text.getLength());
        } else if (data instanceof Room[]) {
            Room[] roomDtos = (Room[]) data;
            byteBuffer.putInt(roomDtos.length);

            int index = start + 4;
            for (int i=0; i< roomDtos.length; i++) {
                byteBuffer.putInt(index, roomDtos[i].getRoomNumber());
                putString(byteBuffer, roomDtos[i].getRoomId(), index + 4, index + 20);
                byteBuffer.putInt(index + 20, roomDtos[i].getNumOfUser());
                byteBuffer.putInt(index + 24, roomDtos[i].getNumOfText());
                index += 28;
            }
            byteBuffer.limit(index);

        } else if (data instanceof List) {
            List list = (List) data;
            if (list.isEmpty()) return;

            byteBuffer.putInt(start, list.size());
            int index = start + 4;

            if (list.get(0) instanceof Room) {
                List<Room> rooms = list;

                for (int i=0; i<rooms.size(); i++) {
                    byteBuffer.putInt(index, rooms.get(i).getRoomNumber());
                    putString(byteBuffer, rooms.get(i).getRoomId(), index + 4, index + 20);
                    byteBuffer.putInt(index + 20, rooms.get(i).getNumOfUser());
                    byteBuffer.putInt(index + 24, rooms.get(i).getNumOfText());
                    index += 28;
                }
            } else if (list.get(0) instanceof String) {
                List<String> s = list;

                for (int i=0; i<s.size(); i++) {
                    putString(byteBuffer, s.get(i), index, index + 16);
                    index += 16;
                }
            }

            byteBuffer.limit(index);
        }
    }

    private static String getString(ByteBuffer byteBuffer, int start, int end) {
        byteBuffer.position(start);

        for (int i=start; i<end; i++) {
            if (byteBuffer.get(i) == 0) {
                end = i;
                break;
            }
        }

        byteBuffer.limit(end);
        String result = charset.decode(byteBuffer).toString();

        byteBuffer.limit(1000);
        return result;
    }



    public static void fileWrite(BroadCastResponse broadCastResponse) {
        try (AsynchronousFileChannel fileChannel = AsynchronousFileChannel.open(Paths.get("./message"), StandardOpenOption.WRITE ,StandardOpenOption.CREATE,StandardOpenOption.READ)) {
            ByteBuffer byteBuffer = makeBuffer(broadCastResponse);

            int filePosition = (int) fileChannel.size();

            Future future = fileChannel.write(byteBuffer, filePosition);
            Future future1 = fileChannel.write(ByteBuffer.wrap(";".getBytes()), filePosition + byteBuffer.limit());

            future.get();
            future1.get();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
