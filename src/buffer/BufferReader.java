package buffer;

import protocol.dto.*;
import protocol.type.BroadCastType;
import protocol.type.RequestType;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class BufferReader {
    private static Charset charset = Charset.forName("UTF-8");

    public static RequestResponse readResponseBuffer(ByteBuffer byteBuffer) {
        int requestId = byteBuffer.getInt(0);

        RequestType requestType = RequestType.findRequestType(byteBuffer.getInt(4));
        int result = byteBuffer.getInt(8);
        Object data = getResponseData(byteBuffer, 12, requestType);

        return new RequestResponse(requestId, requestType, result, data);
    }

    public static BroadCastResponse readBroadCastBuffer(ByteBuffer byteBuffer) {
        int broadCastNumber = byteBuffer.getInt(4);
        int roomNumber = byteBuffer.getInt(8);
        String curId = getString(byteBuffer, 12, 27);

        BroadCastType broadCastType = BroadCastType.findBroadCastType(broadCastNumber);

        String time = getString(byteBuffer, 28, 39);
        Object data = getBroadCastData(byteBuffer, 40, broadCastType);

        return new BroadCastResponse(-1, broadCastType, roomNumber, curId, data, time);
    }

    public static void printByteBuffer(ByteBuffer byteBuffer) {
        for (int i=0; i<byteBuffer.limit(); i++) {
            System.out.print((int)byteBuffer.get(i) + " ");
        }
        System.out.println();
    }

    private static String getString(ByteBuffer byteBuffer, int start, int end) {
        byteBuffer.position(start);

        for (int i=start; i<end; i++) {
            if (byteBuffer.get(i) == 0) {
                end = i;
                break;
            }
        }

        byteBuffer.limit(end);
        String result = charset.decode(byteBuffer).toString();

        byteBuffer.limit(1000);
        return result;
    }

    private static Object getResponseData(ByteBuffer byteBuffer, int start, RequestType requestType) {
        switch (requestType) {
            case CREATEROOM: return byteBuffer.getInt(start);
            case ROOMLIST: {
                int numOfRoom = byteBuffer.getInt(start);
                Room[] rooms = new Room[numOfRoom];

                int index = start + 4;
                for (int i=0; i<numOfRoom; i++) {
                    int roomNumber = byteBuffer.getInt(index);
                    String roomId = getString(byteBuffer, index + 4, index + 20);
                    int numOfUser = byteBuffer.getInt(index + 20);
                    int numOfText = byteBuffer.getInt(index + 24);
                    rooms[i] = new Room(roomNumber, roomId, numOfUser, numOfText);
                    index += 28;
                }

                return rooms;
            }
            case LOGIN:
            case LOGOUT:
            case SEND:
            case QUIT:
            case EXIT:
            case FILEUPLOAD:
            case ENTER:
            case INVITE:
            case FILEREGIST: return null;

            default: throw new IllegalArgumentException("잘못된 requestType!");
        }
    }

    private static Object getBroadCastData(ByteBuffer byteBuffer, int start, BroadCastType broadCastType) {
        switch (broadCastType) {
            case INVITE: {
                int num = byteBuffer.getInt(start);
                String[] data = new String[num];

                int index = start + 4;
                for (int i=0; i<num; i++) {
                    data[i] = getString(byteBuffer, index, index+16);
                    index += 16;
                }

                return data;
            }

            case QUIT: {
                return null;
            }

            case SEND: {
                int textId = byteBuffer.getInt(start);
                int numOfRead = byteBuffer.getInt(start + 4);
                int length = byteBuffer.getInt(start + 8);
                String message = getString(byteBuffer, start + 12, start + 12 + length);
                return new Text(textId, numOfRead, length, message);
            }

            case ENTERROOM: {
                int textStart = byteBuffer.getInt(start);
                int textEnd = byteBuffer.getInt(start + 4);
                return new Pair(textStart, textEnd);
            }
            case FILEUPLOAD: {

            }
            case FILEREMOVE: {

            }
            case SOCKETCLOSE:{
                return null;
            }
            default: throw new IllegalArgumentException("없는 broadcastNumber!");
        }
    }

    public static List<BroadCastResponse> fileRead() {
        Path path = Paths.get("./message");

        try (AsynchronousFileChannel fileChannel = AsynchronousFileChannel.open(path, StandardOpenOption.READ, StandardOpenOption.CREATE)) {
            ByteBuffer byteBuffer = ByteBuffer.allocate((int) fileChannel.size());
            Future future = fileChannel.read(byteBuffer, 0);
            future.get();

            List<BroadCastResponse> responses = new ArrayList<>();

            ByteBuffer buffer = ByteBuffer.allocate(1024*30);

            for (int i=0; i<byteBuffer.limit(); i++) {
                byte b = byteBuffer.get(i);

                if (b == (byte)';') {
                    buffer.position(0);
                    responses.add(BufferReader.readBroadCastBuffer(buffer));
                    buffer.position(0);
                    buffer.limit(1024*30);
                    continue;
                }
                buffer.put(b);
            }

            return responses;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
