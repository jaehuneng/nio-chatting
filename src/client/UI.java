package client;

import protocol.dto.BroadCastResponse;
import protocol.dto.RequestResponse;
import protocol.dto.Room;
import protocol.dto.Text;
import protocol.type.BroadCastType;
import protocol.type.RequestType;

import java.util.Map;

public class UI {
    private Client client = null;

    public UI(Client client) {
        this.client = client;
    }

    public String findRoomName(int roomId) {
        for (Map.Entry<String, Integer> map : client.roomMap.entrySet()) {
            if (map.getValue() == roomId) {
                return map.getKey();
            }
        }
        throw new IllegalArgumentException("없는방을 검색!");
    }

    public void broadCastDisplay(BroadCastResponse broadCastResponse) {
        int roomNumber = broadCastResponse.getRoomNumber();
        if (roomNumber != client.getCurrentRoomNumber() && broadCastResponse.getBroadCastType() != BroadCastType.INVITE) return;
        String userId = broadCastResponse.getCurId();
        Object data = broadCastResponse.getData();

        switch (broadCastResponse.getBroadCastType()) {
            case INVITE: {
                String[] visitor = (String[]) data;
                System.out.print(broadCastResponse.getRoomNumber() + "번 방에서 " + userId + "님이 ");
                for (int i=0; i<visitor.length; i++) {
                    System.out.print(visitor[i]);
                    if (i != visitor.length-1) {
                        System.out.print(", ");
                    }
                }
                System.out.println("를 초대하셨습니다.");
                return;
            }
            case QUIT: {
                System.out.println(userId + "님이 퇴장하셨습니다.");
                break;
            }
            case SEND: {
                Text text = (Text) data;
                System.out.println(userId + ": " + text.getMessage() + " - " + text.getNumOfUnread());
                break;
            }
            case FILEUPLOAD: {
                break;
            }
            case FILEREMOVE: {
                break;
            }
            case ENTERROOM: {
                System.out.println(userId + "님이 입장하셨습니다.");
                break;
            }
            case SOCKETCLOSE: {
                System.out.println(userId + "님의 클라이언트가 종료되었습니다.");
                break;
            }
            default: throw new IllegalArgumentException("잘못된 broadcastType!");
        }
    }

    public void responseDisplay(RequestResponse requestResponse) {
        RequestType requestType = requestResponse.getRequestType();
        int result = requestResponse.getResult();
        Object data = requestResponse.getData();

        switch (requestType) {
            case LOGIN: {
                System.out.println("로그인 result: " + result);
                break;
            }
            case ROOMLIST: {
                Room[] rooms = (Room[]) data;

                System.out.println("-------현재 참여한 방 목록--------");

                for (int i=0; i< rooms.length; i++) {
                    System.out.println(rooms[i].getRoomId() + " 방 | 참가인원: " + rooms[i].getNumOfUser() + "명 | 메세지 수: " + rooms[i].getNumOfText());
                }
                break;
            }
            case CREATEROOM: {
                System.out.println("만들어진 방 번호: " + (Integer) data);
                break;
            }
            case LOGOUT: {
                System.out.println("로그아웃 하셨습니다.");
                break;
            }
            case QUIT: {
                System.out.println("방을 퇴장하셨습니다.");
                break;
            }
        }
    }
}
