package client;

import protocol.dto.*;
import protocol.type.RequestType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Service {
    private static final int RESULT_SUCCESS = 0;
    private static final int UNKNOWN_ERROR = 1;
    private static final int UNKNOWN_COMMAND = 2;
    private static final int NOT_ACCESSIBLE = 3;
    private static final int RESULT_ALREADY_LOG_IN = 4;
    private static final int NO_FILE = 5;

    private UI display = null;
    private Client client = null;

    HashMap<Integer, List<BroadCastResponse>> broadCastMap = new HashMap<>();
    HashMap<Integer, Integer> roomReadId = new HashMap<>();

    public Service (Client client) {
        this.client = client;
        this.display = new UI(client);
    }

    private int requestId = 0;
    public Request makeRequest(String id, RequestType requestType, int roomNumber, Object data) {
        Client.requestMap.put(requestId, requestType);
        return new Request(requestId++, requestType, id, roomNumber, data);
    }

    public void doServiceRequest(RequestResponse requestResponse) {
        if (requestResponse.getResult() != 0) {
            failService(requestResponse);
            return;
        }

        switch (requestResponse.getRequestType()) {
            case LOGIN: loginService(requestResponse); break;
            case LOGOUT: logoutService(); break;
            case ROOMLIST: roomListService(requestResponse); break;
            case ENTER: enterService(requestResponse); break;
            case QUIT: quitRoomService(requestResponse); break;
            case EXIT: exitRoomService(requestResponse); break;
        }

        display.responseDisplay(requestResponse);
    }

    public void doServiceBroadCast(BroadCastResponse broadCastResponse) {
        switch (broadCastResponse.getBroadCastType()) {
            case SEND: putMessageService(broadCastResponse); break;
            case INVITE: inviteService(broadCastResponse); break;
            case ENTERROOM: enterService(broadCastResponse); break;
        }

        display.broadCastDisplay(broadCastResponse);
    }

    public void failService(RequestResponse requestResponse) {
        System.out.println("요청이 실패했습니다.");
    }

    private void loginService(RequestResponse requestResponse) {
        client.setCurrentRoomNumber(-1);
    }

    private void putMessageService(BroadCastResponse broadCastResponse) {
        if (!broadCastResponse.isRead()) {
            //BufferWriter.fileWrite(broadCastResponse);
            if (broadCastMap.isEmpty()) {
                broadCastMap.put(broadCastResponse.getRoomNumber(),Arrays.asList(broadCastResponse));
            } else {
                broadCastResponse.read();
                broadCastMap.get(broadCastResponse.getRoomNumber()).add(broadCastResponse);
            }
        }
    }

    private void roomListService(RequestResponse requestResponse) {
        Room[] rooms = (Room[]) requestResponse.getData();
        Arrays.stream(rooms).forEach(room -> {
            client.roomMap.put(room.getRoomId(), room.getRoomNumber());
            roomReadId.put(room.getRoomNumber(), room.getNumOfText());
        });

    }

    private void enterService(RequestResponse requestResponse) {
        if (requestResponse.getResult() != 0) {
            client.setCurrentRoomNumber(-1);
        } else {
            broadCastMap.computeIfAbsent(client.getCurrentRoomNumber(), k -> new ArrayList<>());

            int index = broadCastMap.get(client.getCurrentRoomNumber()).size();
            for (BroadCastResponse broadCastResponse : broadCastMap.get(client.getCurrentRoomNumber())) {
                Text text = (Text) broadCastResponse.getData();
                index--;
                if (index < roomReadId.get(client.getCurrentRoomNumber())) text.read();
                doServiceBroadCast(broadCastResponse);
            }
        }
    }

    private void enterService(BroadCastResponse broadCastResponse) {
        if (broadCastMap.get(broadCastResponse.getRoomNumber()) != null) {
            Pair pair = (Pair) broadCastResponse.getData();

            if (pair.isSame()) return;

            for (int i = pair.getX(); i < pair.getY(); i++) {
                Text text = (Text) broadCastMap.get(broadCastResponse.getRoomNumber()).get(i).getData();
                text.read();
            }
        }

    }

    private void inviteService(BroadCastResponse broadCastResponse) {
        //client.setCurrentRoomNumber(broadCastResponse.getRoomNumber());  invite 되면 해당 방 변경
        broadCastMap.computeIfAbsent(broadCastResponse.getRoomNumber(), k -> new ArrayList<>());
    }

    public void logoutService() {
        client.send(RequestType.LOGOUT, null);
        client.stopClient();
    }

    private void quitRoomService(RequestResponse requestResponse) {
        client.quitRoom();
    }

    private void exitRoomService(RequestResponse requestResponse) {
        client.exitRoom();
    }
}
