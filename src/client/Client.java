package client;

import buffer.BufferWriter;
import buffer.BufferReader;
import protocol.dto.*;
import protocol.type.RequestType;
import sun.misc.Signal;
import sun.misc.SignalHandler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Client {
    private final Client self = this;
    private String id;
    private String currentRoomName;
    private int currentRoomNumber = -1;
    public HashMap<String, Integer> roomMap = new HashMap<>();
    AsynchronousChannelGroup channelGroup;
    public AsynchronousSocketChannel socketChannel;
    Service service = null;

    public static HashMap<Integer, RequestType> requestMap = new HashMap<>();

    public void startClient(String ip, int port) {
        try {
            channelGroup = AsynchronousChannelGroup.withFixedThreadPool(
                    Runtime.getRuntime().availableProcessors(),
                    Executors.defaultThreadFactory()
            );

            socketChannel = AsynchronousSocketChannel.open(channelGroup);
            socketChannel.connect(new InetSocketAddress(ip, port), null,
                    new CompletionHandler<Void, Void>() {
                        @Override
                        public void completed(Void result, Void attachment) {
                            terminalHandlerActivate();
                            receive();
                        }

                        @Override
                        public void failed(Throwable exc, Void attachment) {
                            if (socketChannel.isOpen()) stopClient();
                        }
                    });



            service = new Service(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopClient() {
        if (channelGroup != null && !channelGroup.isShutdown()) {
            try {
                channelGroup.shutdownNow();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setRoom(String roomId) {
        currentRoomName = roomId;
        setCurrentRoomNumber(roomMap.get(roomId));
    }

    public void quitRoom() {
        roomMap.remove(currentRoomName);
        currentRoomName = null;
        setCurrentRoomNumber(-1);
    }

    public void exitRoom() {
        currentRoomName = null;
        setCurrentRoomNumber(-1);
    }

    public void setCurrentRoomNumber(int roomNum) {
        this.currentRoomNumber = roomNum;
    }

    public int getCurrentRoomNumber() {
        return currentRoomNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void receive() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(1000);

        socketChannel.read(byteBuffer, byteBuffer,
                new CompletionHandler<Integer, ByteBuffer>() {
                    @Override
                    public void completed(Integer result, ByteBuffer attachment) {
                        int requestId = attachment.getInt(0);

                        if (requestId != -1) {
                            RequestResponse requestResponse = BufferReader.readResponseBuffer(attachment);
                            System.out.println("receive: " + requestResponse.toString());
                            service.doServiceRequest(requestResponse);
                        } else {
                            BroadCastResponse broadCastResponse = BufferReader.readBroadCastBuffer(attachment);
                            System.out.println("receive: " + broadCastResponse.toString());

                            service.doServiceBroadCast(broadCastResponse);
                        }

                        receive();
                    }

                    @Override
                    public void failed(Throwable exc, ByteBuffer attachment) {
                        try {
                            socketChannel.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    public void send(RequestType requestType, Object data) {
        Request request = service.makeRequest(this.id, requestType, this.currentRoomNumber, data);
        ByteBuffer byteBuffer = BufferWriter.makeBuffer(request);

        System.out.println("sending: " + request.toString());
        //BufferReader.printByteBuffer(byteBuffer);

        socketChannel.write(byteBuffer, null,
                new CompletionHandler<Integer, Object>() {
                    @Override
                    public void completed(Integer result, Object attachment) {

                    }

                    @Override
                    public void failed(Throwable exc, Object attachment) {

                    }
                });
    }

    String command = null;
    public void send(RequestType requestType, Object data, String command) {
        Request request = service.makeRequest(this.id, requestType, this.currentRoomNumber, data);
        ByteBuffer byteBuffer = BufferWriter.makeBuffer(request);

        System.out.println("sending: " + request.toString());
        //BufferReader.printByteBuffer(byteBuffer);

        socketChannel.write(byteBuffer, null,
                new CompletionHandler<Integer, Object>() {
                    @Override
                    public void completed(Integer result, Object attachment) {

                    }

                    @Override
                    public void failed(Throwable exc, Object attachment) {

                    }
                });
    }

    public void terminalHandlerActivate() {
        SignalHandler terminalSignalHandler = new SignalHandler() {
            @Override
            public void handle(Signal signal) {
                try {
                    System.out.println("로그아웃");
                    Thread.sleep(500);

                    service.logoutService();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Signal.handle(new Signal("INT"), terminalSignalHandler);
    }
}
