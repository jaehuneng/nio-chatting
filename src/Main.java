import client.Client;
import protocol.type.RequestType;
import sun.misc.Signal;
import sun.misc.SignalHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Main {
    //static AsynchronousSocketChannel socketChannel;
    static Client client = new Client();
    public static void main(String[] args) throws IOException, InterruptedException {
        client.startClient("localhost", 5225);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String command = "";

        System.out.print("로그인 아이디를 입력하세요 : ");
        command = bufferedReader.readLine();
        client.setId(command);

        client.send(RequestType.LOGIN, null);

        while ((command = bufferedReader.readLine()) != null) {
            if (command.charAt(0) == '/') {
                String[] commands = command.split(" ");
                if ("/listroom".equals(commands[0])) {
                    client.send(RequestType.ROOMLIST, null);
                } else if ("/enter".equals(commands[0])) {
                    client.setRoom(commands[1]);
                    client.send(RequestType.ENTER, null, commands[1]);
                } else if ("/create".equals(commands[0])) {
                    client.send(RequestType.CREATEROOM, commands[1]);
                } else if ("/invite".equals(commands[0])) {
                    client.send(RequestType.INVITE, Arrays.stream(commands).filter(s -> !"/invite".equals(s)).collect(Collectors.toList()));
                } else if ("/quit".equals(commands[0])) {
                    client.send(RequestType.QUIT, null);
                } else if ("/exit".equals(commands[0])) {
                    client.send(RequestType.EXIT, null);
                } else if ("/logout".equals(commands[0])) {
                    client.send(RequestType.LOGOUT, null);
                }
            } else {
                client.send(RequestType.SEND, command);
            }
        }


    }
}
